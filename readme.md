Brown Rabbit
============

This is the job test if you wish to be a web developer at HTML24

Completed
--------

Apply responsivness, make sure that the page look good on the following media queries:

* Mobile - 375px
* Tablet - 768px
* Desktop - 1200px

Search functionality:

* You should be able to search through the content of the page and highlight the result

Slider:

* Start the slider with a random image every time the page is refreshed 

Read more button:

* Use a click event to show the rest of the text 
