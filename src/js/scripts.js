$(function() {

//stop carousel from sliding
$('.carousel').carousel('pause')

// array with image url's
var imageURLs = [
    "img/carousel/coffee.png"
  , "img/carousel/beer.png"
  , "img/carousel/cups.jpg"

];

// loads a random image in the carousel
$(document).ready(function() {
    var img = '<img src=\"';
    var randomIndex = Math.floor(Math.random() * imageURLs.length);
    img += imageURLs[randomIndex];
    img += '\" class="d-block w-100">';
    $('.carousel-item').children().replaceWith(img)
    return img;
  })

    // define max visible characters allowed 
    var showChar = 30;
    var ellipsestext = "...";

    $('.text').each(function() {
      var text = $(this)
      var content = $(this).html();
      
        if(content.length > showChar) {
  
            var visible = content.substr(0, showChar);
            var hidden = content.substr(showChar, content.length - showChar);

            var html = visible + '<span class="dots">' + ellipsestext + '</span>' + '<span class="hidden moretext">' + hidden + '</span>';
        
            $(this).html(html);

        }
    })
        // show the rest of the text when "Read more" button is clicked
        $(document).on('click', ".showmore", function() {

          var a = $(this).parent().siblings().find('.moretext')
          var d = $(this).parent().siblings().find('.dots')
          a.removeClass('hidden')
          d.addClass('hidden')
          $(this).addClass("showless")
          $(this).removeClass("showmore")
          $(this).html("Read less")

          
      })
        // hide the text when "Read less" is clicked
        $(document).on('click', ".showless", function() {

          var b = $(this).parent().siblings().find('.moretext')
          var dd = $(this).parent().siblings().find('.dots')
          b.addClass('hidden')
          dd.removeClass('hidden')
          $(this).addClass("showmore")
          $(this).removeClass("showless")
          $(this).html("Read more")

      })
    
      // mark.js library for highlighting the keyword in page
      // https://stackoverflow.com/a/37343967
      var mark = function() {

        // Read the keyword
        var keyword = $("input[name='keyword']").val();    
        // Remove previous marked elements and mark
        // the new keyword inside the context
        $(".context").unmark({
          done: function() {
            $(".context").mark(keyword);
          }
        });
      };
    
      $("input[name='keyword']").on("input", mark);
    
    });